﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter()
    {
        gameObject.SetActive(false);
        if (gameObject.name.Equals("Obstacle1"))
        {
            gameObject.transform.parent = GameObject.Find("Obstacle1Pool").transform;
        }
        else if (gameObject.name.Equals("Obstacle2"))
        {
            gameObject.transform.parent = GameObject.Find("Obstacle2Pool").transform;
        }
        else if (gameObject.name.Equals("Obstacle3"))
        {
            gameObject.transform.parent = GameObject.Find("Obstacle3Pool").transform;
        }
    }
}
