﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour
{
    public int _lane = 3;

    Vector2 _firstPressPos;
    Vector2 _secondPressPos;
    Vector2 _currentSwipe;
    float _minSwipeLength = 200f;
    public enum Swipe { None, Up, Down, Left, Right };
    public static Swipe swipeDirection;

    // Use this for initialization
    void Start () {
	    
	}

    // Update is called once per frame
    void Update()
    {
        //transform.position = new Vector3(transform.position.x, Mathf.PingPong(Time.time * 2, 2) + 6.5f, transform.position.z);
        if (Input.touches.Length > 0)
        {
            Touch t = Input.GetTouch(0);
            if (t.phase == TouchPhase.Began)
            {
                //save began touch 2d point
                _firstPressPos = new Vector2(t.position.x, t.position.y);
            }
            if (t.phase == TouchPhase.Ended)
            {
                //save ended touch 2d point
                _secondPressPos = new Vector2(t.position.x, t.position.y);

                //create vector from the two points
                _currentSwipe = new Vector3(_secondPressPos.x - _firstPressPos.x, _secondPressPos.y - _firstPressPos.y);

                // Make sure it was a legit swipe, not a tap
                if (_currentSwipe.magnitude < _minSwipeLength)
                {
                    swipeDirection = Swipe.None;
                    return;
                }

                //normalize the 2d vector
                _currentSwipe.Normalize();

                //swipe upwards
                if (_currentSwipe.y > 0 && _currentSwipe.x > -0.5f && _currentSwipe.x < 0.5f)
                {
                    Debug.Log("up swipe");
                }
                //swipe down
                if (_currentSwipe.y < 0 && _currentSwipe.x > -0.5f && _currentSwipe.x < 0.5f)
                {
                    Debug.Log("down swipe");
                }
                //swipe left
                if (_currentSwipe.x < 0 && _currentSwipe.y > -0.5f && _currentSwipe.y < 0.5f)
                {
                    Debug.Log("left swipe");
                    if (!GameManager.Instance._moving)
                    {
                        transform.position = new Vector3(transform.position.x - 2, transform.position.y, transform.position.z);
                        _lane--;
                    }
                }
                //swipe right
                if (_currentSwipe.x > 0 && _currentSwipe.y > -0.5f && _currentSwipe.y < 0.5f)
                {
                    Debug.Log("right swipe");
                    if (!GameManager.Instance._moving)
                    {
                        transform.position = new Vector3(transform.position.x + 2, transform.position.y, transform.position.z);
                        _lane++;
                    }
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            if (!GameManager.Instance._moving)
            {
                transform.position = new Vector3(transform.position.x - 2, transform.position.y, transform.position.z);
                _lane--;
            }
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            if (!GameManager.Instance._moving)
            {
                transform.position = new Vector3(transform.position.x + 2, transform.position.y, transform.position.z);
                _lane++;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.transform.name);
    }
}
