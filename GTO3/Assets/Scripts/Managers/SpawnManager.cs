﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnManager : MonoBehaviour {

    public int maxWorldLength = 3;
    private int curWorldLength = 0;

    public string[] leftColors;
    public string[] rightColors;
    public string[] upColors;
    public string[] downColors;

    public int _randomEncounters = 10;

    List<Transform> _spawnedObstacles = new List<Transform>();

    System.Random _rnd = new System.Random();

    public int waitForTiles = 24;
	private int _curWaitForTiles = 0;

    public GameObject _lastSpawned;

    int _curTilePos = -4;


    GameObject GameWorld;

    //ObjectPools
    ObjectPool _tilesPool;
    ObjectPool _sideBlocksPool;
    ObjectPool _obstacle1Pool;
    ObjectPool _obstacle2Pool;
    ObjectPool _obstacle3Pool;
    ObjectPool _coinPool;

    bool _first = true;

    private static SpawnManager _instance;

    public static SpawnManager instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<SpawnManager>();
            return _instance;
        }
    }

    // Use this for initialization
    void Start()
    {
        
    }

    /// <summary>
    /// Waits for the right amount of blocks passed, then generates new blocks
    /// </summary>
    /// <param name="size">Decreases world size with this number</param>
    public void DecreaseCurrentWorldLength(int size)
    {
		if (_curWaitForTiles == waitForTiles - 1) {
			_curWaitForTiles = 0;
            curWorldLength -= size;
            StartCoroutine(Generate());
		}
		_curWaitForTiles++;
    }

    /// <summary>
    /// Will generate the world when this method is called
    /// for as long as the worldlength is smaller than maximum world length
    /// </summary>
    public IEnumerator Generate()
    {
        float blockLength = 0;
        while(GameManager.Instance._worldmoving)
        {
            yield return null;
        }
        GameWorld = GameManager.Instance.GameWorld;
        _tilesPool = ObjectPoolManager.Instance.getPoolByName("Tiles");
        _sideBlocksPool = ObjectPoolManager.Instance.getPoolByName("SideBlocks");

        Transform side = null;
        while (curWorldLength < maxWorldLength)
        {
            for(int i = 0; i<GameWorld.transform.childCount; i++)
            {
                _spawnedObstacles.Clear();
                for (int j = 0; j < GameWorld.transform.GetChild(i).childCount; j++)
                {
                    side = GameWorld.transform.GetChild(i);
                    if (!side.GetChild(j).name.Equals("Obstacles"))
                    {
                        if (!side.GetChild(j).name.Equals("Coins"))
                        {
                            ObjectPool _tempPool = _tilesPool;
                            if (side.GetChild(j).name.Equals("Side"))
                                _tempPool = _sideBlocksPool;

                            Transform temp = _tempPool.getNext();
                            temp.name = "Tile";


                            //if lane is side, name the block. otherwise set the blocklength to the lenght of a tile
                            if (side.GetChild(j).name.Equals("Side"))
                                temp.name = "SideBlock";
                            else
                                blockLength = temp.localScale.z;

                            temp.parent = side.GetChild(j);

                            if (!_first)
                            {
                                temp.localPosition = new Vector3(_curTilePos, 0, _lastSpawned.transform.position.z);
                                temp.localEulerAngles = new Vector3(0, 0, 0);
                            }
                            else if (_first)
                            {
                                temp.localPosition = new Vector3(_curTilePos, 0, 0);
                                temp.localEulerAngles = new Vector3(0, 0, 0);
                                _first = false;
                            }

                            //Sets the color of the material depending on the side it is on
                            if (!temp.name.Equals("SideBlock"))
                            {
                                if (!side.GetChild(j).name.Equals("Obstacles"))
                                {
                                    if (!side.GetChild(j).name.Equals("Coins"))
                                    {
                                        GenerateObstacles(side.FindChild("Obstacles"));

                                        //Switch colors based on lane
                                        string _switchLaneColor = side.name;
                                        switch (_switchLaneColor)
                                        {
                                            case "Up":
                                                temp.GetComponent<Renderer>().material.color = hexToColor(upColors[j]);
                                                break;
                                            case "Right":
                                                temp.GetComponent<Renderer>().material.color = hexToColor(rightColors[j]);
                                                break;
                                            case "Down":
                                                temp.GetComponent<Renderer>().material.color = hexToColor(downColors[j]);
                                                break;
                                            case "Left":
                                                temp.GetComponent<Renderer>().material.color = hexToColor(leftColors[j]);
                                                break;
                                            default:
                                                Debug.Log("Error");
                                                break;
                                        }
                                    }
                                }
                            }
                            _curTilePos += 2;
                            _lastSpawned.transform.position = temp.position;
                        }
                    }
                }
                GenerateCoins(side.FindChild("Coins"));
                _curTilePos = -4;
            }
            _lastSpawned.transform.position += new Vector3(0, 0, blockLength);
            curWorldLength++;
        }
        //Stops itself
        StopCoroutine(Generate());
    }

    // Update is called once per frame
    void Update () {

	
	}

    /// <summary>
    /// Generates randomly spawned blocks
    /// </summary>
    public void GenerateObstacles(Transform parent)
    {
        _obstacle1Pool = ObjectPoolManager.Instance.getPoolByName("Obstacle1Pool");
        _obstacle2Pool = ObjectPoolManager.Instance.getPoolByName("Obstacle2Pool");
        _obstacle3Pool = ObjectPoolManager.Instance.getPoolByName("Obstacle3Pool");

        ObjectPool[] pools = new ObjectPool[] { _obstacle1Pool, _obstacle2Pool, _obstacle3Pool };
        
        int rand = _rnd.Next(_randomEncounters);
        if(rand < 3)
        {
            Transform obstacle = pools[rand].getNext();
            obstacle.transform.parent = parent;
            obstacle.localPosition = new Vector3(_curTilePos, 1.5f, _lastSpawned.transform.position.z);
            
            _spawnedObstacles.Add(obstacle);
            obstacle.localEulerAngles = new Vector3(0, 0, 0);
        }
    }

    public void GenerateCoins(Transform parent)
    {
        if (_spawnedObstacles.Count > 0)
        {
            _coinPool = ObjectPoolManager.Instance.getPoolByName("CoinPool");
            Transform coin = _coinPool.getNext();

            List<float> freePoints = new List<float>() { -4, -2, 0, 2, 4 };

            for (int i = 0; i < _spawnedObstacles.Count; i++)
            {
                for (int j = 0; j < freePoints.Count; j++)
                {
                    if (_spawnedObstacles[i].position.x.Equals(freePoints[j]))
                    {
                        freePoints.Remove(freePoints[j]);
                    }
                }
            }

            int next = _rnd.Next(freePoints.Count);

            coin.transform.parent = parent;
            coin.transform.localPosition = new Vector3(freePoints[next], 1.5f, _spawnedObstacles[0].position.z);
        }
    }

    /// <summary>
    /// Transforms hex string to color object
    /// </summary>
    /// <param name="hex">Hex string</param>
    /// <returns>Color</returns>
    public static Color hexToColor(string hex)
    {
        hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
        hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
        byte a = 255;
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        return new Color32(r, g, b, a);
    }
}