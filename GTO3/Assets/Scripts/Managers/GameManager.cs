﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{

    public GameObject GameWorld;
    public GameObject Player;
    private PlayerScript _playerScript;

    private Vector3 _moveOrigin, _moveFirstTarget, _moveTargetFinal;
    private Quaternion _worldOrigin, _worldTarget;
    private float _moveSpeed = 2f;
    private float _timerPos, _timerPosBall;

    public bool _moving = false, _worldmoving = false, halfway = false;

    private static GameManager _instance;
    private bool _ready;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GameManager>();
            return _instance;
        }
    }

    // Use this for initialization
    void Start()
    {
        _playerScript = Player.GetComponent<PlayerScript>();
    }

    public void Ready()
    {
        StartCoroutine(SpawnManager.instance.Generate());
        _ready = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate()
    {
        if (_ready)
        {
            foreach (Transform side in GameWorld.transform)
            {
                foreach(Transform lane in side)
                {
                    foreach(Transform block in lane)
                    {
                        block.Translate(new Vector3(0,0,-4) * Time.deltaTime, Space.Self);
                    }
                }
            }
            if (!_moving && !_worldmoving)
            {
                if (_playerScript._lane == 6)
                {
                    _moveOrigin = Player.transform.position;
                    _moveFirstTarget = _moveOrigin - new Vector3(4f, 0, 0) + new Vector3(0, 3.5f, 0);
                    _moveTargetFinal = _moveFirstTarget - new Vector3(6f, 3.5f, 0);
                    _worldOrigin = GameWorld.transform.rotation;
                    _worldTarget = _worldOrigin * Quaternion.Euler(0, 0, 90f);
                    _moving = true;
                    _worldmoving = true;
                    _playerScript._lane = 1;
                }
                else if (_playerScript._lane == 0)
                {
                    _moveOrigin = Player.transform.position;
                    _moveFirstTarget = _moveOrigin + new Vector3(4f, 0, 0) + new Vector3(0, 3.5f, 0);
                    _moveTargetFinal = _moveFirstTarget + new Vector3(6f, -3.5f, 0);
                    _worldOrigin = GameWorld.transform.rotation;
                    _worldTarget = _worldOrigin * Quaternion.Euler(0, 0, -90f);
                    _moving = true;
                    _worldmoving = true;
                    _playerScript._lane = 5;
                }
            }
            if (_moving)
            {
                _timerPosBall = Mathf.Clamp(_timerPosBall + Time.deltaTime * 2, 0.0f, 1.0f / _moveSpeed);
                if (!halfway)
                {
                    Player.transform.position = Vector3.Lerp(_moveOrigin, _moveFirstTarget, _moveSpeed * _timerPosBall);
                    if (Player.transform.position == _moveFirstTarget)
                    {
                        _timerPosBall = 0;
                        halfway = true;
                    }
                }
                else
                {
                    Player.transform.position = Vector3.Lerp(_moveFirstTarget, _moveTargetFinal, _moveSpeed * _timerPosBall);
                    if (Player.transform.position == _moveTargetFinal)
                    {
                        _timerPosBall = 0;
                        halfway = false;
                        _moving = false;
                    }
                }
            }
            if (_worldmoving)
            {
                _timerPos = Mathf.Clamp(_timerPos + Time.deltaTime, 0.0f, 1.0f / _moveSpeed);
                GameWorld.transform.rotation = Quaternion.Lerp(_worldOrigin, _worldTarget, _moveSpeed * _timerPos);
                if (GameWorld.transform.rotation == _worldTarget)
                {
                    _timerPos = 0;
                    _worldmoving = false;
                }
            }
        }
    }
}
