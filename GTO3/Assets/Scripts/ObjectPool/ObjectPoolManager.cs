﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPoolManager : MonoBehaviour
{
    public List<ObjectPool> ObjectPools = new List<ObjectPool>();

    private Dictionary<string, ObjectPool> pools = new Dictionary<string, ObjectPool>();
    private Dictionary<string, GameObject> containers = new Dictionary<string, GameObject>();
    
    private static ObjectPoolManager _instance;

    public static ObjectPoolManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<ObjectPoolManager>();
            return _instance;
        }
    }
    
    // Use this for initialization
    void Start ()
    {
        foreach (ObjectPool pool in ObjectPools)
        {
            GameObject container;
            if (pool.alternateParent != null)
                container = pool.alternateParent;
            else
            {
                container = new GameObject();
                container.name = pool.name;
                container.transform.parent = transform;
            }

            pool.parent = container.transform;

            pools.Add(pool.name, pool);
            containers.Add(pool.name, container);

            pool.generate();
        }
        Debug.Log("Object pools ready");
        GameManager.Instance.Ready();
	}

    public ObjectPool getPoolByName(string name)
    {
        foreach (KeyValuePair<string, ObjectPool> pool in pools)
        {
            if (pool.Key.Equals(name))
                return pool.Value;
        }

        return null;
    }

    public GameObject createGameObject(GameObject prefab, Vector3 position, Quaternion rotation)
    {
        GameObject t = (GameObject)Instantiate(prefab, position, rotation);
        return t;
    }

    
}
