﻿using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]
public class ObjectPool
{
    public string name;
    public GameObject prefab;
    public int size;
    public bool adaptive;
    public GameObject alternateParent;

    [HideInInspector]
    public Transform parent;
    
    private List<GameObject> objects = new List<GameObject>();

    public void generate()
    {
        for (int i = 0; i < size; i++)
        {
            GameObject obj = ObjectPoolManager.Instance.createGameObject(prefab, Vector3.zero, Quaternion.identity);
            objects.Add(obj);
            objects[i].transform.parent = parent;
            objects[i].SetActive(false);
        }
    }

    public Transform getNext()
    {
        for (int i = 0; i < objects.Count; i++)
        {
            if(objects[i].activeInHierarchy == false)
            {
                objects[i].SetActive(true);
                return objects[i].transform;
            }
        }

        if(adaptive)
        {
            GameObject obj = ObjectPoolManager.Instance.createGameObject(prefab, Vector3.zero, Quaternion.identity);
            objects.Add(obj);
            obj.transform.parent = parent;
            obj.SetActive(true);
            return obj.transform;
        }

        return null;
    }
}
