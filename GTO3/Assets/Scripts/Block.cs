﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.name.Equals("Remover"))
        {
            gameObject.SetActive(false);
            if (gameObject.name.Equals("SideBlock"))
            {
                gameObject.transform.parent = GameObject.Find("SideBlocks").transform;
            }
            else if (gameObject.name.Equals("Tile"))
            {
                gameObject.transform.parent = GameObject.Find("Tiles").transform;
            }
            SpawnManager.instance.DecreaseCurrentWorldLength(1);
        }
    }
}
